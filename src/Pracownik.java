import java.time.LocalDate;


public class Pracownik extends Czlowiek {
   private LocalDate dataZatr;

public LocalDate getDataZatr() {
	return dataZatr;
}
   public Pracownik(String imie,String nazwisko,LocalDate dataUr,LocalDate dataZatr){
	   super(imie,nazwisko,dataUr);
	   this.dataZatr=dataZatr;
   }
}
